<?php
// =============================== TS Request Quote  Widget ======================================
class TS_RequestQuoteWidget extends WP_Widget {
    /** constructor */
    function TS_RequestQuoteWidget() {
        parent::WP_Widget(false, $name = 'TS - Request a Quote');	
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
		$txt1 = apply_filters('widget_txt1', $instance['txt1']);
		$txt2 = apply_filters('widget_txt2', $instance['txt2']);
		$txt3 = apply_filters('widget_txt2', $instance['txt3']);
        ?>
              <?php echo $before_widget; ?>
						<div class="col-l">
							<h2><?php echo $title; ?></h2>
							<p><?php echo $txt1; ?></p>
						</div><!-- end col-l -->
						<div class="col-r">
						<?php if($txt2!="" && $txt3!=""){ ?>
							<a href="<?php echo $txt3; ?>" class="but"><?php echo $txt2; ?></a>
						<?php } ?>
						</div><!-- end col-r -->
						<div class="clr"></div><!-- clear float -->
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {				
        $title = esc_attr($instance['title']);
		$txt1 = esc_attr($instance['txt1']);
		$txt2 = esc_attr($instance['txt2']);
		$txt3 = esc_attr($instance['txt3']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'minibuzz'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
			 <p><label for="<?php echo $this->get_field_id('txt1'); ?>"><?php _e('Text:', 'minibuzz'); ?><textarea rows="5"  class="widefat" id="<?php echo $this->get_field_id('txt1'); ?>" name="<?php echo $this->get_field_name('txt1'); ?>"><?php echo $txt1; ?></textarea></label></p>
			 <p><label for="<?php echo $this->get_field_id('txt2'); ?>"><?php _e('Button Text', 'minibuzz'); ?> <input class="widefat" id="<?php echo $this->get_field_id('txt2'); ?>" name="<?php echo $this->get_field_name('txt2'); ?>" type="text" value="<?php echo $txt2; ?>" /></label></p>
			 <p><label for="<?php echo $this->get_field_id('txt3'); ?>"><?php _e('Url', 'minibuzz'); ?> <input class="widefat" id="<?php echo $this->get_field_id('txt3'); ?>" name="<?php echo $this->get_field_name('txt3'); ?>" type="text" value="<?php echo $txt3; ?>" /></label></p>
        <?php 
    }

} // class Request Quote Widget
