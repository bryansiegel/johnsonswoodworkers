<?php get_header(); ?>
<div id="header-inner">
  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
  <?php /* If this is a category archive */ if (is_category()) { ?>
	<h1 class="pagetitle">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
	<h1 class="pagetitle">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
	<h1 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h1>
  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
	<h1 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h1>
  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
	<h1 class="pagetitle">Archive for <?php the_time('Y'); ?></h1>
  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
	<h1 class="pagetitle">Author Archive</h1>
  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
	<h1 class="pagetitle">Blog Archives</h1>
  <?php } ?>
</div><!-- end #header-inner -->
<div id="content">
	<div id="content-left">
		<div id="maintext">
			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div id="breadcrumbs">','</div>');
			} ?>
		<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?>>
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<span class="smalltext"><?php _e('Posted by', 'minibuzz');?> <?php the_author() ?> <?php _e('on', 'minibuzz');?> <?php the_time('F j, Y') ?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php edit_post_link(__('Edit', 'minibuzz'), '', '&nbsp;&nbsp;|&nbsp;&nbsp;'); ?> <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span>

				<div class="entry">
					<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
				</div>
			</div>

		<?php endwhile; ?>

			 <?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagination">
				 <?php wp_pagenavi(); ?>
				 </div>	
			 <?php }else{ ?>
				<div class="navigation">
					<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries', 'minibuzz')) ?></div>
					<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;', 'minibuzz')) ?></div>
				</div>
			<?php }?>
			
		<?php else :
			
			if (is_category()): ?> 
				<h2 class='center'> <?php printf(__("Sorry, but there aren't any posts in the %s category yet.", 'minibuzz'),single_cat_title('',false)); ?> </h2> 
				<?php elseif (is_date()): ?> 
				<h2 class='center'> <?php _e("Sorry, but there aren't any posts within this date."); ?> </h2> 
				<?php elseif (is_author()): $userdata = get_userdatabylogin(get_query_var('author_name')); ?>
				<h2 class='center'> <?php printf(__("Sorry, but there aren't any posts by %s yet.", 'minibuzz'),$userdata->display_name); ?> </h2> 
				<?php else: ?> 
				<h2 class='center'> <?php _e('No posts found.', 'minibuzz'); ?> </h2>
			<?php  endif;?>
			
		<?php  endif;?>

		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-blog-right.php'); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->

<?php get_footer(); ?>
