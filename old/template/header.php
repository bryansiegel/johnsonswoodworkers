<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php 
$favicon = get_option('minibuzz_favicon');
if($favicon =="" ){
?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
<?php }else{?>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
<?php }?>
<!-- ////////////////////////////////// -->
<!-- //      Start Stylesheets       // -->
<!-- ////////////////////////////////// -->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href="<?php bloginfo('stylesheet_directory');?>/jqueryslidemenu.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('stylesheet_directory');?>/s3slider.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('stylesheet_directory');?>/fancy.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('stylesheet_directory');?>/colorbox.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/styles/theme_css.php" rel="stylesheet"  type="text/css" />
<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/fade.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/contact-form.js"></script>
<!-- dropdown menu -->
<script type="text/javascript">
//Specify full URL to down and right arrow images (23 is padding-right to add to top level LIs with drop downs):
var arrowimages={down:['downarrowclass', '<?php bloginfo('stylesheet_directory');?>/images/down.gif', 38], right:['rightarrowclass', '<?php bloginfo('stylesheet_directory');?>/images/right.gif']}
</script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/jqueryslidemenu.js"></script>
<!-- Header slideshow -->
<?php if(is_home() || is_front_page()) { ?>
<?php $time = get_option('minibuzz_slider_timeout') ;?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jqFancyTransitions.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/s3Slider.js"></script>
<script type="text/javascript">
var $ = jQuery.noConflict();
    $(document).ready(function() {
        $('#s3slider').s3Slider({
            timeOut:<?php echo $time ;?>
        });
    });
</script>
<?php } ?>
<!-- cycle jquery -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript">
	$('.boxslideshow').cycle({
	timeout: 6000,  // milliseconds between slide transitions (0 to disable auto advance)
	fx:      'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...            
	pause:   0,	  // true to enable "pause on hover"
	pauseOnPagerHover: 0 // true to pause when hovering over pager link
	});
</script>
<!-- colorbox jquery -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.colorbox.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//Examples of how to assign the ColorBox event to elements
		$(".image").colorbox();
		$(".video").colorbox({iframe:true, innerWidth:425, innerHeight:344});
	});
</script>
<!-- cufont style -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/Tuffy_500-Tuffy_700-Tuffy_italic_500.font.js"></script>
<script type="text/javascript">
	 Cufon.replace('h1') ('h1 a') ('h2') ('h3') ('h4') ('h5') ('h6') ('.desc') ('.largetext')
</script>
<!--[if IE 6]>
<script src="<?php bloginfo('stylesheet_directory');?>/js/DD_belatedPNG.js"></script>
<script>
  DD_belatedPNG.fix('img');
</script>
<![endif]--> 

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body>

<div id="wrapper">
	<div id="container">
		<div id="top">
			<?php
			$logotype = get_option('minibuzz_logo_type');
			$logoimage = get_option('minibuzz_logo_image'); 
			$sitename = get_option('minibuzz_site_name');
			$tagline = get_option('minibuzz_tagline');
			if($logoimage == ""){ $logoimage = get_bloginfo('stylesheet_directory') . "/images/logo.png"; }
			?>
			<?php if($logotype == 'textlogo'){ ?>
			<div id="logo">
					<?php if($sitename=="" && $tagline==""){?>
							<h1><a href="<?php echo get_option('home'); ?>/" title="<?php _e('Click for Home','minibuzz'); ?>"><?php bloginfo('name'); ?></a></h1>
						<span class="desc"><?php bloginfo('description'); ?></span>
					<?php }else{ ?>
						<h1><a href="<?php echo get_option('home'); ?>/" title="<?php _e('Click for Home','minibuzz'); ?>"><?php echo $sitename; ?></a></h1>
						<span class="desc"><?php echo $tagline; ?></span>
					<?php }?>
			</div><!-- end #logo -->
			<?php } else { ?>
			<div id="logoimage">
				<a href="<?php echo get_option('home'); ?>/"><img src="<?php echo $logoimage ?>" alt="" height="100" /></a>
			</div>
			<?php }?>
			
			<?php $searchposition = get_option('minibuzz_search_position');?>
			<?php if($searchposition == 'above'){?>
			<div id="topsearch-menu">
				 <?php include_once (TEMPLATEPATH . '/searchform.php'); ?>
			</div><!-- end #topsearch -->
			<?php }?>
		</div><!-- end #top -->
		<div id="topnavigation">
		
				
					<?php if($searchposition == 'default'){
					$navigation = "topnav";
					}else{
					$navigation = "topnav-full";
					}
					?>
		
			<div id="<?php echo $navigation;?>">
					<?php 
							if(is_array(get_option('minibuzz_menu'))){
								$menu = implode(",",get_option('minibuzz_menu'));
							}else{
								$menu = "";
							}
								
					 ?>
					<div id="myslidemenu" class="jqueryslidemenu">
					<ul>
						<li class="home<?php if (is_front_page() || is_home()) echo ' current_page_item'; ?>">
							<a href="<?php echo get_option('home'); ?>/">Home</a>
						</li>
						<?php wp_list_pages('title_li=0&depth=3&sort_column=menu_order&exclude=' . $menu ); ?>
					</ul>
					</div>
			</div><!-- end #topnav -->
			<?php if($searchposition == 'default'){?>
			<div id="topsearch">
				 <?php include_once (TEMPLATEPATH . '/searchform.php'); ?>
			</div><!-- end #topsearch -->
			<?php }?>
		</div><!-- end #topnavigation -->
		<?php if(is_home() || is_front_page()){?>
				<?php include_once (TEMPLATEPATH . '/slider.php'); ?>
		<?php } ?>
