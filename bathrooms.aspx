﻿<%@ Page Title="Riverside Custom Bathroom Cabinets | Johnsons Woodworkers" Language="C#" MasterPageFile="~/MasterPages/SiteInternal.master" AutoEventWireup="true" CodeFile="bathrooms.aspx.cs" Inherits="bathrooms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Riverside Custom Bathroom Cabinet maker Johnsons Woodworkers has 35 years of custom Bathroom cabinet experience. If you're looking to reface your bathroom cabinets and need a free estimate contact us today!" />
<style type="text/css">
        .style1
        {
            width: 416px;
            height: 324px;
        }
        .style2
        {
            width: 367px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" Runat="Server">
<h1>Custom Bathroom Cabinets</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <table cellpadding="10" class="boxslideshow">
        <tr>
            <td class="style2" valign="top">
                <img alt="" class="style1" 
                    src="http://www.johnsonswoodworkers.com/images/bathroom-cabinets/bathroom-cabinet-main.jpg" /></td>
            <td valign="top">
                <a class="thumbnail" href="#thumb"><img id="Img1" src="images/bathroom-cabinets/bathroom-cabinet-1small.jpg" width="100px" height="66px" border="0" /><span><img id="Img2" src="images/bathroom-cabinets/bathroom-cabinet-1big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img3" src="images/bathroom-cabinets/bathroom-cabinet-2small.jpg" width="100px" height="66px" border="0" /><span><img id="Img4" src="images/bathroom-cabinets/bathroom-cabinet-2big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img5" src="images/bathroom-cabinets/bathroom-cabinet-3small.jpg" width="100px" height="66px" border="0" /><span><img id="Img6" src="images/bathroom-cabinets/bathroom-cabinet-3big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img7" src="images/bathroom-cabinets/bathroom-cabinet-4small.jpg" width="100px" height="66px" border="0" /><span><img id="Img8" src="images/bathroom-cabinets/bathroom-cabinet-4big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img9" src="images/bathroom-cabinets/bathroom-cabinet-5small.jpg" width="100px" height="66px" border="0" /><span><img id="Img10" src="images/bathroom-cabinets/bathroom-cabinet-5big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img11" src="images/bathroom-cabinets/bathroom-cabinet-6small.jpg" width="100px" height="66px" border="0" /><span><img id="Img12" src="images/bathroom-cabinets/bathroom-cabinet-6big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img13" src="images/bathroom-cabinets/bathroom-cabinet-7small.jpg" width="100px" height="66px" border="0" /><span><img id="Img14" src="images/bathroom-cabinets/bathroom-cabinet-7big.jpg" /><br />So real, it's unreal. Or is it?</span></a>
</td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidePanel" Runat="Server">
<h2>Make over your Bathroom with new  cabinets</h2>
<p>Looking for a Bathroom makeover? Consider Custom Bathroom Cabinets. Choose your design, wood type and color--and turn that bathroom from drab to fantastic.</p>
</asp:Content>

