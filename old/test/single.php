<?php get_header(); ?>
<div id="content">
	<div id="content-left">
		<div id="maintext">
			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div id="breadcrumbs">','</div>');
			} ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h2><?php the_title(); ?></h2>
					<span class="smalltext"><?php _e('Posted by', 'minibuzz');?> <?php the_author() ?> <?php _e('on', 'minibuzz');?> <?php the_time('F j, Y') ?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php edit_post_link(__('Edit', 'minibuzz'), '', '&nbsp;&nbsp;|&nbsp;&nbsp;'); ?> <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span>
					<div class="entry">
						<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
						<?php wp_link_pages(array('before' => '<p><strong>' . __('Pages', 'minibuzz') . ':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
						<?php the_tags( '<p>'. __('Tags', 'minibuzz'). ': ', ', ', '</p>'); ?>
		
					</div>
				</div>
		
			<?php comments_template(); ?>
		
			<?php endwhile; else: ?>
		
				<p><?php _e('Sorry, no posts matched your criteria.', 'minibuzz');?></p>
		
		<?php endif; ?>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	
	<div id="content-right">
		<div id="sideright">
			<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-blog-right.php'); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->
<?php get_footer(); ?>
