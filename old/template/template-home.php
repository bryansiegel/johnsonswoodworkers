<?php /*
Template Name: Home
*/
?>
<?php get_header(); ?>
<div id="content">
	<div id="content-left">
		<div id="maintext">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
		<div class="entry">
			<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
		</div>
		<?php endwhile; endif; ?><br style="clear:left" />
		<?php edit_post_link(__('Edit this entry.', 'minibuzz'), '<p>', '</p>'); ?>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-home-right.php'); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->

<?php get_footer(); ?>
