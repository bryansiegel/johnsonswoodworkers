<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US"> 
<head profile="http://gmpg.org/xfn/11"> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<title>Contact Us</title> 
<link rel="pingback" href="http://demo.templatesquare.com/wordpress/minibuzz/xmlrpc.php" /> 
<link rel="shortcut icon" href="http://demo.templatesquare.com/wordpress/minibuzz/wp-content/themes/minibuzz/images/favicon.ico" /> 
 
<!-- ////////////////////////////////// --> 
<!-- //      Start Stylesheets       // --> 
<!-- ////////////////////////////////// --> 
<link href="colorbox.css" rel="stylesheet" type="text/css" />
<link href="fancy.css" rel="stylesheet" type="text/css" />
<link href="jqueryslidemenu.css" rel="stylesheet" type="text/css" />
<link href="rtl.css" rel="stylesheet" type="text/css" />
<link href="s3slider.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
<!-- ////////////////////////////////// --> 
<!-- //      Javascript Files        // --> 
<!-- ////////////////////////////////// --> 
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script> 
<script type="text/javascript" src="js/fade.js"></script> 
<script type="text/javascript" src="js/contact-form.js"></script> 
<!-- dropdown menu --> 
<!-- cufont style --> 
<script type="text/javascript" src="js/cufon-yui.js"></script> 
<script type="text/javascript" src="js/Tuffy_500-Tuffy_700-Tuffy_italic_500.font.js"></script> 
<script type="text/javascript"> 
	 Cufon.replace('h1') ('h1 a') ('h2') ('h3') ('h4') ('h5') ('h6') ('.desc') ('.largetext')
</script> 
<!--[if IE 6]>
<script src="js/DD_belatedPNG.js"></script>
<script>
  DD_belatedPNG.fix('img');
</script>
<![endif]--> 
 
<script type='text/javascript' src='js/swfobject.js'></script>
</head> 
<body> 
 
<div id="wrapper"> 
	<div id="container"> 
		<div id="top"> 
									<div id="logoimage"> 
				<a href="http://www.wassersonlaw.com"><img src="images/logo.jpg" alt="" height="100" /></a> 
			</div> 
						
								</div><!-- end #top --> 
		<div id="topnavigation"> 
		
				
							
			<div id="topnav"> 
										<div id="myslidemenu" class="jqueryslidemenu"> 
					<ul> 
						<li class="home current_page_item"> 
							<a href="http://www.wassersonlaw.com" title="Upland Divorce Attorney">Home</a> 
						</li> 
						<li class="page_item page-item-6"><a href="Overview.shtml" title="Firm Overview">Firm Overview</a>  
</li> 
<li class="page_item page-item-9"><a href="PracticeAreas.shtml" title="Contact">Practice Areas</a></li> 
<li class="page_item page-item-7"><a href="Collaborative-Divorce.shtml" title="Portfolio">Divorce</a></li> 
<li class="page_item page-item-8"><a href="Probate-Estate-Planning.shtml" title="Blog">Probate Estate Planning</a></li> 
<li class="page_item page-item-9"><a href="DynamicAttorneys.shtml" title="Attorney Profile">About</a></li> 
<li class="page_item page-item-9"><a href="Contact.php" title="Contact">Contact</a></li> 


					</ul> 
					</div> 
			</div><!-- end #topnav --> 
						<div id="topsearch"> 
				 
<p style="color:#ba5409; font-size:medium; font-weight:bold;">(909) 920-0440</p> 
		</div><!-- end #topsearch --> 
					</div><!-- end #topnavigation --> 
		<div id="header-inner"> 
				<h1 class="pagetitle">Contact Us</h1> 
			</div><!-- end #header-inner --> 
<div id="content"> 
	<div id="content-left"> 
		<div id="maintext"> 
				<form action="gdform.php" method="post">

					<table align="center" style="width: 60%">
						<tr>
							<td style="width: 165px"><strong>Name:</strong></td>
							<td><input name="Name: " type="text" />&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 165px"><strong>Telephone:</strong></td>
							<td><input name="Telephone: " type="text" />&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 165px"><strong>Email:</strong></td>
							<td><input name="Email: " type="text" />&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 165px"><strong>Comments/Questions:</strong></td>
							<td><textarea cols="20" name="Comments/Questions: " rows="2"></textarea>&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 165px">&nbsp;</td>
							<td>
							<input name="Submit1" type="submit" value="submit" />&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 165px">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>

				</form>
				<br />
				
<!-- You can start editing here. --> 
 
 
			<!-- If comments are closed. --> 
		<!-- <p class="nocomments">Comments are closed.</p> --> 
 
	
 
		</div><!-- end #maintext --> 
	</div><!-- end #content-left --> 
	<div id="content-right"> 
		<div id="sideright"> 
			<div class="box"><ul><li id="flexipages-2" class="widget flexipages_widget"><h2 class="widgettitle">
				Contact Us</h2> 
				  <li><br />
				  <a href="http://demo.templatesquare.com/wordpress/minibuzz/?p=101" rel="bookmark" title="Permanent Link to Lorem ipsum dolor sit amet">
				  <strong>(909) 920-0440</strong></a><br />818 North Mountain Avenue #104<br />
Upland, CA 91786							
				  </li>
																              </ul></div>                      
		</div><!-- end #sideright --> 
	</div><!-- end #content-right --> 
	<div class="clr"></div><!-- end clear float --> 
</div><!-- end #content --> 
 
		<div id="footer"> 
			<div id="footer-text"> 
									Copyright &copy;
			2010 The Law Office of Samuel R. Wasserson, Inc.. All rights reserved. 	
						</div><!-- end #footer-text --> 
						<p>The use of the Internet for communications with the firm will not establish an attorney-client relationship and messages containing confidential or time-sensitive information should not be sent.
The Law Office of Samuel R. Wasserson, Inc., located in Upland, California, represents clients in Upland, Rancho Cucamonga, Pomona, Alta Loma, Ontario, Claremont, La Verne, Chino, the Central District of Los Angeles, and the Inland Empire area of Southern California, including Riverside County and San Bernardino County.</p>
<p>The information you obtain at this site is not, nor is it intended to be, legal advice. You should consult an attorney for individual advice regarding your own situation.</p>

		</div><!-- end #footer --> 
	</div><!-- end #container -->	
</div><!-- end #wrapper -->	
</body> 
</html> 