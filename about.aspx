﻿<%@ Page Title="About Johnsons Woodworkers - 35 years of Custom Cabinet Making" Language="C#" MasterPageFile="~/MasterPages/SiteInternal.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="about" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Johnsons Woodworkers is a Riverside California custom woodworking company. Contact Johnsons woodworkers for a free estimate" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" Runat="Server">
    <h1 class="auto-yle1st">About Johnsons Woodworkers</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <p style="color:#000; font-size:x-large;">35 Years and Counting...</p>
						<p>Johnsons Woodworkers was started by Keith Johnson in his garage. After taking a 
                            side job of building a few cabinets Keith instantly relized that he could make a 
                            living out of it. Since then Johnsons Woodworkers has built first class Kitchen 
                            Cabinets, Entertainment Centers, along with custom wood work.</p>
    <p>Family owned and operated Johnsons Woodworkers has built a solid foundation of 
        satisfied clients that come back year after year for custom wood work. Johnson&#39;s 
        Woodworkers believes in building a product for you that will last, while 
        complementing your dream.</p>
    <p>If you live anywhere within the Inland Empire and Orange County contact Johnsons 
        Woodworkers today for a FREE Estimate!</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidePanel" runat="server">


</asp:Content>

