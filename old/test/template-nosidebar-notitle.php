<?php /*
Template Name: No Sidebar No Title
*/
?>
<?php get_header(); ?>
<div id="content-full">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="entry">
			<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
		</div>
		<?php endwhile; endif; ?><br style="clear:left" />
		<?php edit_post_link(__('Edit this entry.', 'minibuzz'), '<p>', '</p>'); ?>
</div><!-- end #content-full -->
<?php get_footer(); ?>
