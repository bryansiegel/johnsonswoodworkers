		<div id="footer">
			<div id="footer-text">
			<?php $foot= stripslashes(get_option('minibuzz_footer'))?>
			<?php if($foot==""){?>
			<?php _e('Copyright', 'minibuzz'); ?> &copy;
			<?php global $wpdb;
			$post_datetimes = $wpdb->get_results("SELECT YEAR(min(post_date_gmt)) AS firstyear, YEAR(max(post_date_gmt)) AS lastyear FROM $wpdb->posts WHERE post_date_gmt > 1970");
			if ($post_datetimes) {
				$firstpost_year = $post_datetimes[0]->firstyear;
				$lastpost_year = $post_datetimes[0]->lastyear;

				$copyright = $firstpost_year;
				if($firstpost_year != $lastpost_year) {
					$copyright .= '-'. $lastpost_year;
				}
				$copyright .= ' ';

				echo $copyright;
				bloginfo('name');
			}
		?>. <?php _e('All rights reserved.', 'minibuzz'); ?>
		
			<?php }else{?>
			<?php echo $foot; ?>
			<?php } ?>
			</div><!-- end #footer-text -->
		</div><!-- end #footer -->
	</div><!-- end #container -->	
</div><!-- end #wrapper -->	
<?php wp_footer(); ?>
<?php $google = stripslashes(get_option('minibuzz_google'));?>
<?php if($google==""){?>
<?php }else{?>
<?php echo $google; ?>
<?php } ?>
</body>
</html>
