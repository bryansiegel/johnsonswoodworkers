<?php get_header(); ?>
<div id="content">
	<div id="content-left">
		<div id="maintext">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<span><?php _e('Posted by', 'minibuzz');?> <?php the_author() ?> <?php _e('on', 'minibuzz');?> <?php the_time('F j, Y') ?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php edit_post_link(__('Edit', 'minibuzz'), '', '&nbsp;&nbsp;|&nbsp;&nbsp;'); ?> <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span>
					<div class="entry">
						<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
					</div>
				</div><!-- end post -->
	
			<?php endwhile; ?>
	
			 <?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagination">
				 <?php wp_pagenavi(); ?>
				 </div>	
			 <?php }else{ ?>
				<div class="navigation">
					<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries', 'minibuzz')) ?></div>
					<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;', 'minibuzz')) ?></div>
				</div>
			<?php }?>
	
		<?php else : ?>
			<h2 class="center"><?php _e('Not Found', 'minibuzz');?></h2>
			<p class="center"><?php _e('Sorry, but you are looking for something that isnt here', 'minibuzz');?></p>
			<?php include_once (TEMPLATEPATH . '/searchform.php'); ?>
		<?php endif; ?>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	
	<div id="content-right">
		<div id="sideright">
			<?php get_sidebar();?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->
<?php get_footer(); ?>
