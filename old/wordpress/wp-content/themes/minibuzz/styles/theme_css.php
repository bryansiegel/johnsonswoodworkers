<?php 
require('../../../../wp-blog-header.php');

$bgcolor = get_option('minibuzz_bgcolor');
$bgimage = get_option('minibuzz_bgimage');
$hcolor = get_option('minibuzz_hcolor');
$topline = get_option('minibuzz_topline');
$pagetitlecolor = get_option('minibuzz_pagetitle');
$linkcolor = get_option('minibuzz_linkcolor');
$disableslidertext= get_option('minibuzz_disable_slider_text');


if($bgcolor==""){ $bgcolor = "#ebeceb"; }
if($hcolor==""){ $hcolor = "#2d84b6"; }
if($topline==""){ $topline = "#2d84b6"; }
if($linkcolor==""){ $linkcolor = "#df7034"; }


header('Content-type: text/css');
?> 

body{background-color:<?php echo $bgcolor; ?>; <?php if($bgimage!=""){ ?>background-image:url(<?php echo $bgimage; ?>)<?php } ?>}
a, a:visited{color:<?php echo $linkcolor; ?>;}
h1, h2, h3, h4, h5, h6, h1 a, h1 a:visited, h2 a, h2 a:visited, h3 a, h3 a:visited, h4 a, h4 a:visited, h5 a, h5 a:visited, h6 a, h6 a:visited, span.tblue,
span.tblue a, span.tblue a:visited, span.tblue2
{color:<?php echo $hcolor; ?> !important}
#searchresult .post h3 a{color:<?php echo $hcolor; ?> !important;}
#top{border-top:solid 2px <?php echo $topline; ?>;}
code{border-left:solid 5px <?php echo $topline; ?>;}
#topnav ul li.current_page_item a, #topnav-full ul li.current_page_item a{border-top:solid 3px <?php echo $topline; ?>;}
#topnav ul li.current_page_item ul li a, #topnav ul li ul li.current_page_item a, #topnav-full ul li.current_page_item ul li a, #topnav-full ul li ul li.current_page_item a{border-top:0px !important;}
#sideright ul li.recentcomments a, #sideright ul li.recentcomments a:visited,
.boxslideshow a, .boxslideshow a:visited
{color:<?php echo $linkcolor; ?> !important;}

#header-inner h1.pagetitle{color:<?php echo $pagetitlecolor; ?> !important;}
.pagination a{color:#656253; }
.pagination .current{color:<?php echo $linkcolor; ?>;}
<?php if($disableslidertext==true){ ?>
.s3sliderImage div {background-color:transparent;}
<?php } else { ?>
.s3sliderImage div {background-color:#000;}
<?php }?>