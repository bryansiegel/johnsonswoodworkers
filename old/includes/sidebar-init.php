<?php
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name'          => 'Sidebar',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Home Right',
		'id'         	=> 'home-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title'	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar About Right',
		'id'         	=> 'about-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title'	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Portfolio Navigation',
		'id'         	=> 'portfolio-navigation',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li>',
		'before_title'	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Portfolio Bottom',
		'id'         	=> 'portfolio-bottom',
		'before_widget' => '<div id="bottombox">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Blog Right',
		'id'         	=> 'blog-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Contact Right',
		'id'         	=> 'contact-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Footer1',
		'id'         	=> 'footer1',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Extra1 Right',
		'id'         	=> 'extra1-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Extra2 Right',
		'id'         	=> 'extra2-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Sidebar Extra3 Right',
		'id'         	=> 'extra3-right',
		'before_widget' => '<div class="box"><ul><li id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</li></ul></div>',
		'before_title' 	=> '<h2 class="widgettitle">',
		'after_title' 	=> '</h2>',
	));
}
?>