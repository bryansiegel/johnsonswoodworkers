	<?php $slidetype = get_option('minibuzz_slider_type');?>
	<?php $time = get_option('minibuzz_slider_timeout') ;?>
	<?php $effect = get_option('minibuzz_slider_effect') ;?>
	<?php $position = get_option('minibuzz_slider_position') ;?>
	<?php $direction = get_option('minibuzz_slider_direction') ;?>
	<?php $strip = get_option('minibuzz_slider_strip') ;?>
	<?php if($slidetype == 'slider1' || $slidetype == 'slider2'){?>
	<div id="header">
		<div id="slideshow">
		 <?php if($slidetype == 'slider1'){?>
		  <div id="s3slider">
				<ul id="s3sliderContent">
				<?php 
				$getcat = get_option('minibuzz_slider_category');
				if($getcat=="" || $getcat=="All Categories"){ $querycat = "&category_name="; }else{ $querycat = "&category_name=" . $getcat; }
				?>
				<?php query_posts("showposts=-1" . $querycat);?>
				<?php while (have_posts()) : the_post(); ?>	
				<li class="s3sliderImage">
					<?php $disabletimthumb= get_option('minibuzz_disable_slider_timthumb') ;?>
					<?php $disableslidertext= get_option('minibuzz_disable_slider_text') ;?>
					<?php $image = get_post_custom_values("slider-image");?>
					<?php $url = get_post_custom_values("slider-url");?>
					<?php if($disabletimthumb==true){ ?>
					<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>"  width="940" height="340"  />
					<?php } else {?>
<img src="<?php echo bloginfo('template_url'); ?>/includes/timthumb.php?src=/<?php echo $image[0]; ?>&amp;w=940&amp;h=340&amp;zc=1&amp;q=100" alt="<?php the_title();  ?>" width="940" height="340"  />						<?php }?>
					<div>
					<?php if($disableslidertext!=true){ ?>
						<h1><?php the_title();?></h1>
						<?php $excerpt = get_the_excerpt();
						echo string_limit_words($excerpt,200).'...';?>
						<?php if($url!=""){ ?>
						<br /><br /><a href="<?php echo $url[0]; ?>" class="but"><?php _e('Read more','minibuzz');?></a>
						<?php }?>
					<?php }?>
					</div>
				</li>
				<?php endwhile; ?>
				<?php wp_reset_query();?>
				<li class="clear s3sliderImage"></li>
				</ul>
			</div>
			<?php }?>
			<?php if($slidetype == 'slider2'){?>
			<div id="slideflash">
				<div id="slideshowHolder">
				<?php 
				$getcat = get_option('minibuzz_slider_category');
				if($getcat=="" || $getcat=="All Categories"){ $querycat = "&category_name="; }else{ $querycat = "&category_name=" . $getcat; }
				?>
				<?php query_posts("showposts=-1" . $querycat);?>
				<?php while (have_posts()) : the_post(); ?>	
					<?php $disabletimthumb= get_option('minibuzz_disable_slider_timthumb') ;?>
					<?php $disableslidertext= get_option('minibuzz_disable_slider_text') ;?>
					<?php $image = get_post_custom_values("slider-image");?>
					<?php $url = get_post_custom_values("slider-url");?>
					
					<?php if($disabletimthumb==true){ ?>
					<img src="<?php echo $image[0]; ?>" alt="<?php if($disableslidertext!=true){ ?><?php $excerpt = get_the_excerpt();
						echo string_limit_words($excerpt,200).'...';?><?php }?>"  width="940" height="340"  />
					<?php } else {?>
<img src="<?php echo bloginfo('template_url'); ?>/includes/timthumb.php?src=/<?php echo $image[0]; ?>&amp;w=940&amp;h=340&amp;zc=1&amp;q=100" alt="<?php if($disableslidertext!=true){ ?><?php $excerpt = get_the_excerpt();
						echo string_limit_words($excerpt,200).'...';?><?php }?>" width="940" height="340"  />
					<?php }?>
				<?php endwhile; ?>
				<?php wp_reset_query();?>
				</div><!-- end #slideshowHolder -->
				<script type="text/javascript">
				var $ = jQuery.noConflict();
				$("#slideshowHolder").jqFancyTransitions({ 
				width: 940, // width of panel
				height: 340, // height of panel
				effect: "<?php echo $effect;?>", // wave, zipper, curtain
				position: "<?php echo $position;?>", // top, bottom, alternate, curtain
				strips: <?php echo $strip ;?>, // number of strips
				delay: <?php echo $time ;?>, // delay between images in ms
				stripDelay: 50, // delay beetwen strips in ms
				titleSpeed: 1000, // speed of title appereance in ms
				titleOpacity: 0.7, // opacity of title
				direction: "<?php echo $direction;?>" // left, right, alternate, random, fountain, fountainAlternate
				});
				</script>
			</div><!-- end #slideflash -->
			<?php } ?>
		</div><!-- end slideshow -->
	</div><!-- end #header -->
	<?php }?>
