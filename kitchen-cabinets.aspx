﻿<%@ Page Title="Riverside Custom Kitchen Cabinets, Kitchen Cabinet Refacing | Johnsons Woodworkers" Language="C#" MasterPageFile="~/MasterPages/SiteInternal.master" AutoEventWireup="true" CodeFile="kitchen-cabinets.aspx.cs" Inherits="kitchens" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Riverside California custom kitchen cabinet maker Johnsons Woodworkers can help make your dream kitchen into a reality. Contact Johnsons woodworkers whether you want new wood kitchen cabinets or just kitchen cabinet refacing. Contact us for a free estimate" />
    <style type="text/css">
        .style1
        {
            width: 416px;
            height: 324px;
        }
        .style2
        {
            width: 367px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" Runat="Server">
    <h1>Build Your Dream Kitchen with our Cabinets</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">


    <table cellpadding="10" class="boxslideshow">
        <tr>
            <td class="style2" valign="top">
                <img alt="" 
                    src="images/kitchen-cabinets/custom-kitchen-cabinet.jpg" /></td>
            <td valign="top">
               <a class="thumbnail" href="#thumb"><img id="Img1" src="images/kitchen-cabinets/kitchen-cabinet-2small.jpg" width="100px" height="66px" border="0" /><span><img id="Img2" src="images/kitchen-cabinets/kitchen-cabinet-2big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img3" src="images/kitchen-cabinets/kitchen-cabinet-3small.jpg" width="100px" height="66px" border="0" /><span><img id="Img4" src="images/kitchen-cabinets/kitchen-cabinet-3big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img5" src="images/kitchen-cabinets/kitchen-cabinet-4small.jpg" width="100px" height="66px" border="0" /><span><img id="Img6" src="images/kitchen-cabinets/kitchen-cabinet-4big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img7" src="images/kitchen-cabinets/kitchen-cabinet-5small.jpg" width="100px" height="66px" border="0" /><span><img id="Img8" src="images/kitchen-cabinets/kitchen-cabinet-5big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img9" src="images/kitchen-cabinets/kitchen-cabinet-6small.jpg" width="100px" height="66px" border="0" /><span><img id="Img10" src="images/kitchen-cabinets/kitchen-cabinet-6big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img11" src="images/kitchen-cabinets/kitchen-cabinet-7small.jpg" width="100px" height="66px" border="0" /><span><img id="Img12" src="images/kitchen-cabinets/kitchen-cabinet-7big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img13" src="images/kitchen-cabinets/kitchen-cabinet-8small.jpg" width="100px" height="66px" border="0" /><span><img id="Img14" src="images/kitchen-cabinets/kitchen-cabinet-8big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img15" src="images/kitchen-cabinets/kitchen-cabinet-9small.jpg" width="100px" height="66px" border="0" /><span><img id="Img16" src="images/kitchen-cabinets/kitchen-cabinet-9big.jpg" /><br />So real, it's unreal. Or is it?</span></a>
</td>
        </tr>
    </table>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidePanel" Runat="Server">
    <h2>Kitchens Built to Last</h2>
<p>Are you looking to redo your kitchen cabinets? Why should you go with a cheap brand when you can have a kitchen built the way that you want it, not a cardboard cutout of a kitchen made from a hardware store. With Johnsons Woodworkers you get a kitchen made to last!</p>
<h2>Save with Kitchen Refacing!</h2>
<p>Save money with Kitchen Cabinet Refacing. Instead of replacing all of your cabinets we can make the outside look like new! Not only does this save you money, but time.</p>
</asp:Content>

