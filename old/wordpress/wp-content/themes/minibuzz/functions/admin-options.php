<?php

// Theme Setting
	$themename 				= "MiniBuzz Theme";
	$shortname 				= "minibuzz";
	
// Options panel
$searchheader = array(
				'default' => 'Default',
				'disable' => 'Disable',
				'above' => 'Above Menu'
				  );
			  
$logotype 	= array(
			'textlogo' => 'Text-based logo',
			'imagelogo' => 'Image logo'
   			 );
			  
$slider		= array(
			'slider1' => 'Default',
			'slider2' => 'jQuery Fancy Transition',
			'slider3' => 'Disable'
   			);
			  
$effect = array(
			'wave' => 'Wave',
			'zipper' => 'Zipper',
			'curtain' => 'Curtain'
   			);
			
$position = array(
			'alternate' => 'Alternate',
			'bottom' => 'Bottom',
			'curtain' => 'Curtain',
			'top' => 'Top'
   			);
			
$direction = array(
			'alternate' => 'Alternate',
			'fountain' => 'Fountain',
			'fountainAlternate' => 'FountainAlternate',
			'left' => 'Left',
			'random' => 'Random',
			'right' => 'Right',
   			);
// Setting header
$options = array (
				array(	"name" => "General",
											"type" => "open"),
											
				array(	"name" => __('Theme Color Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('', 'minibuzz')),
		
				array(	"name" => __('Background color', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_bgcolor",
											"std" => "#ebeceb",
											"type" => "colorpicker"),	
											
									
				array(	"name" => __('Heading color', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_hcolor",
											"std" => "#2d84b6",
											"type" => "colorpicker"),												
				

				array(	"name" => __('Top line color', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_topline",
											"std" => "#2d84b6",
											"type" => "colorpicker"),	
											

				array(	"name" => __('Page Title color', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_pagetitle",
											"std" => "#444133",
											"type" => "colorpicker"),
				

				array(	"name" => __('Link color', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_linkcolor",
											"std" => "#df7034",
											"type" => "colorpicker"),
											
				array(	"name" => __('Background Image Url', 'minibuzz'),
											"desc" =>__('Enter the background image url (http://www.fullurl.com/background.png).', 'minibuzz'),
											"id" => $shortname."_bgimage",
											"std" => "",
											"type" => "text"),												
												
				array(	"name" => __('Header Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('', 'minibuzz')),
									
											
				array(	"name" => __('Logo Type', 'minibuzz'),
											"desc" => __('If text-based logo is activated, enter the sitename and tagline in the fields below.', 'minibuzz'),
											"options" => $logotype,
											"id" => $shortname."_logo_type",
											"std" => "textlogo",
											"type" => "select"),
				
				
				array(	"name" => __('Site name', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_site_name",
											"std" => "",
											"type" => "text"),												
				
																	
				array(	"name" => __('Tagline', 'minibuzz'),
											"desc" => '',
											"id" => $shortname."_tagline",
											"std" => "",
											"type" => "text"),	
											
											
				array(	"name" => __('Logo Image URL', 'minibuzz'),
											"desc" => __('If image logo is activated, enter the logo image url (http://www.fullurl.com/logo.png).', 'minibuzz'),
											"id" => $shortname."_logo_image",
											"std" => "",
											"type" => "text"),
											
				array(	"name" => __('Favicon URL', 'minibuzz'),
											"desc" => __('Enter the favicon image url (http://www.fullurl.com/favicon.ico).', 'minibuzz'),
											"id" => $shortname."_favicon",
											"std" => "",
											"type" => "text"),

				array(	"name" => __('Search position', 'minibuzz'),
											"desc" => __('Select search position.', 'minibuzz'),
											"options" => $searchheader,
											"id" => $shortname."_search_position",
											"std" => "default",
											"type" => "select"),
				
				array(	"name" => __('Footer Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('Footer and Analytics Settings', 'minibuzz')),
											
																	
				array(	"name" => __('Footer Text', 'minibuzz'),
											"desc" => __('You can use html tag in here.', 'minibuzz'),
											"id" => $shortname."_footer",
											"std" => "",
											"type" => "textarea"),
		
				array(	"name" => __('Google Analytics', 'minibuzz'),
											"desc" => __('Enter the full google analytics code here.', 'minibuzz'),
											"id" => $shortname."_google",
											"std" => "",
											"type" => "textarea"),
											
											
				array(	"type" => "close"),	
				
				array(	"name" => "Navigation",
											"type" => "open"),
											
											
				array(	"name" => __('Navigation Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('', 'minibuzz')),									
											
				array(	"name" => __('Menu exclude', 'minibuzz'),
											"desc" => __('Select the pages to exclude from main menu.', 'minibuzz'),
											"id" => $shortname."_menu",
											"std" => "",
											"type" => "checkbox-pages"),
											
				array(	"type" => "close"),	
				
				array(	"name" => "Slider",
											"type" => "open"),
									
				array(	"name" => __('Slider Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('', 'minibuzz')),
											
				array(	"name" => __('Slider Type', 'minibuzz'),
											"desc" => __('Select slider type that you would like to have displayed in the slideshow section on your homepage.', 'minibuzz'),
											"options" => $slider,
											"id" => $shortname."_slider_type",
											"std" => "slider1",
											"type" => "select"),
											
				array( 	"name" => __('Slider Category', 'minibuzz'),
											"desc" => __('Select the category that you would like to have displayed in the slider section on your homepage.', 'minibuzz'),
											"id" => $shortname."_slider_category",
											"std" => "Select a category:",
											"type" => "select-categories"),

				array( 	"name" => __('Slider Timeout', 'minibuzz'),
											"desc" => __('Please enter number for slider timeout(transition time). Default is 6000', 'minibuzz'),
											"id" => $shortname."_slider_timeout",
											"std" => "6000",
											"type" => "text"),
								
									
				array(	"name" => __('Disable Timthumb?', 'minibuzz'),
											"desc" => __('Select this checkbox to disable timthumb, the slider image size should 940 x 340 px.', 'minibuzz'),
											"id" => $shortname."_disable_slider_timthumb",
											"std" => "",
											"type" => "checkbox"),
											
				array(	"name" => __('Disable Slider Text', 'minibuzz'),
											"desc" => __('Select this checkbox to disable the slider text.', 'minibuzz'),
											"id" => $shortname."_disable_slider_text",
											"std" => "",
											"type" => "checkbox"),
											
				array(	"name" => __('jQuery Fancy Transition Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('Settings for jquery fancy transition slider.', 'minibuzz')),
											
				array(	"name" => __('Effect', 'minibuzz'),
											"desc" => __('Select effect type for slideshow.', 'minibuzz'),
											"options" => $effect,
											"id" => $shortname."_slider_effect",
											"std" => "zipper",
											"type" => "select"),
											
				array(	"name" => __('Position', 'minibuzz'),
											"desc" => __('Select position type for slideshow.', 'minibuzz'),
											"options" => $position,
											"id" => $shortname."_slider_position",
											"std" => "alternate",
											"type" => "select"),
											
				array(	"name" => __('Direction', 'minibuzz'),
											"desc" => __('Select direction type for slideshow.', 'minibuzz'),
											"options" => $direction,
											"id" => $shortname."_slider_direction",
											"std" => "fountain",
											"type" => "select"),
											
				array(	"name" => __('Strip', 'minibuzz'),
											"desc" => __('number of strips effect.', 'minibuzz'),
											"id" => $shortname."_slider_strip",
											"std" => "40",
											"type" => "text"),
								
				array(	"type" => "close"),	
				
				array(	"name" => "Pages",
											"type" => "open"),

																		
				array(	"name" => __('Posts Count Settings', 'minibuzz'),
											"type" => "heading",
											"desc" => __('', 'minibuzz')),
											
				array(	"name" => __('Blog Page Post Count', 'minibuzz'),
											"desc" => __('Number of posts to be displayed in blog page', 'minibuzz'),
											"id" => $shortname."_blogpost",
											"std" => "3",
											"type" => "text"), 

																		
				array(	"name" => __('Portfolio Page Post Count', 'minibuzz'),
											"desc" => __('Number of posts to be displayed in portfolio page', 'minibuzz'),
											"id" => $shortname."_portfoliopost",
											"std" => "3",
											"type" => "text"),
													
				array(	"name" => "Close",
											"type" => "close"),
											
);


		function mytheme_add_admin() {
	    global $themename, $shortname, $options;
	    if ( $_GET['page'] == basename(__FILE__) ) {
	      if ( 'save' == $_REQUEST['action'] ) {
		      foreach ($options as $value) {
		      	update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
		      foreach ($options as $value) {
		      	if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }
		      header("Location: themes.php?page=admin-options.php&saved=true");
		      die;
	      }
	    }
	    add_theme_page($themename." Options", "Theme Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');
		}
		
		function mytheme_admin() {
	
	    global $themename, $shortname, $options;
	
	    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
	    
?>

	<div class="wrap">
	<div id="icon-themes" class="icon32"><br></div>
	<h2><?php echo $themename; ?> <?php _e('Options','minibuzz');?></h2>
	<div class="bordertitle"></div>
	<style type="text/css">
	table, td {font-size:13px; }
	th {font-weight:normal; width:250px;}
	span.setting-description { font-size:11px; line-height:16px; font-style:italic;}
	</style>
	
	
	<link rel="stylesheet" media="screen" type="text/css" href="<?php bloginfo('template_directory'); ?>/functions/colorpicker/css/colorpicker.css" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/functions/colorpicker/js/colorpicker.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/functions/colorpicker/js/eye.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/functions/colorpicker/js/utils.js"></script>
	<link rel="stylesheet" media="screen" type="text/css" href="<?php bloginfo('template_directory'); ?>/functions/tabs/tabs.css" />
	<!-- Javascript for the tabs -->
	<script type="text/javascript">
	var $ = jQuery.noConflict();
		$(document).ready(function(){
			/* For Tab */
			$(".tab_content").hide(); //Hide all content
			$("ul.tabs li:first").addClass("active").show(); //Activate first tab
			$(".tab_content:first").show(); //Show first tab content
			//On Click Event
			$("ul.tabs li").click(function() {
				$("ul.tabs li").removeClass("active"); //Remove any "active" class
				$(this).addClass("active"); //Add "active" class to selected tab
				$(".tab_content").hide(); //Hide all tab content
				var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
				$(activeTab).fadeIn(900); //Fade in the active content
				return false;
			});
	});	
	</script><br />
	<ul class="tabs"> 
		<li><a href="#General"><?php _e('General','minibuzz'); ?></a></li>
		<li><a href="#Navigation"><?php _e('Navigation','minibuzz'); ?></a></li>
		<li><a href="#Slider"><?php _e('Slider','minibuzz'); ?></a></li>
		<li><a href="#Pages"><?php _e('Pages','minibuzz'); ?></a></li>
	</ul> 

	<div class="tab_container">
		<form method="post">
		<?php 
			foreach ($options as $value) {
			if ($value['type'] == "open") { 
		?>
		 
		 <div id="<?php echo $value['name']; ?>" class="tab_content" >
		<table  border="1" cellpadding="0" cellspacing="12" style="text-align:left">
		<?php
				}
				if ($value['type'] == "close") { 
		?>
		</table></div>
		<?php
				}
				if ($value['type'] == "heading") { 
		?>
		<thead>
		<tr>
        	<td colspan="2"><h3><?php echo $value['name']; ?></h3><span class="setting-description"><?php echo $value['desc']; ?></span></td>
        </tr>
		</thead>
		<?php
				}
				
				if ($value['type'] == "description") { 
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
					<span class="setting-description"><?php echo $value['desc']; ?></span>
		    </td>
		</tr>
		<?php
				}
				
				if ($value['type'] == "info") { 
		?>
		<tr valign="top"> 
		    <th scope="row" colspan="2"><span class="setting-description"><?php echo $value['desc']; ?></span></th>

		</tr>
		<?php
				}
				if ($value['type'] == "line") { 
		?>
		<tr valign="top"> 
		    <th colspan="2" ><div style="padding-top:10px;padding-bottom:10px; vertical-align:middle; padding-left:0px;"><div style="border-bottom: 1px solid #efefef;"></div></div></th>

		</tr>
		
		<?php
				}
				
				
				if ($value['type'] == "text") { 
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
		        <input name="<?php echo $value['id']; ?>" size="60" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" /><br />
 
						<span class="setting-description"><?php echo $value['desc']; ?></span>
		    </td>
		</tr>
		<?php
				}
				
				
				if ($value['type'] == "textarea") { 
				
		?>
		
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
			<textarea cols="50" rows="5"  name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings($value['id']));; } else { echo $value['std']; } ?></textarea><br /><span class="setting-description"><?php echo $value['desc']; ?></span>

		    </td>
		</tr>
		<?php
				}
			if ($value['type'] == "checkbox") { 
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
			<? if(get_settings($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>
                            <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
                            <br /><span class="setting-description"><?php echo $value['desc']; ?></span>
		    </td>
		</tr>
		<?php
				}
			if ($value['type'] == "checkbox-pages") { 
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:<br /><span class="setting-description"><?php echo $value['desc']; ?></span></th>
		    <td>
			<?php 
			$pages_list = get_pages();

			
			?>
			
<table><?php $i = 0; foreach ($pages_list as $option) { $checked = ""; ?><?php if (get_settings( $value['id'])) { if (in_array($option->ID, get_settings( $value['id'] ))) $checked = "checked=\"checked\""; } elseif ($value['std'][$i] == "true") { $checked = "checked=\"checked\""; } ?><tr><td><input type="checkbox" name="<?php echo $value['id']; ?>[]" id="<?php echo $value['id']; ?>-<?php echo $option->ID; ?>" value="<?php echo $option->ID; ?>" <?php echo $checked; ?> /> <label for="<?php echo $value['id']; ?>-<?php echo $option->ID; ?>"><?php echo is_array($value['desc'])?$value['desc'][$i]:$option->post_title; ?></label> </td></tr> <?php $i++; } ?></table>
		    </td>
		</tr>
		<?php
				}

				if ($value['type'] == "select") { 
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
						<select style="width:140px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $key => $option) { ?><option<?php if ( get_settings( $value['id'] ) == $key) { echo ' selected="selected"'; } elseif ($key == $value['std']) { echo ' selected="selected"'; } ?> value="<?php echo $key; ?>"><?php echo $option; ?></option><?php } ?></select> <br /><span class="setting-description"><?php echo $value['desc']; ?></span>
		    </td>
		</tr>
		<?php
				}
				
				
				if ($value['type'] == "colorpicker") { 
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
            <script language="javascript">
            	(function($){
					var initLayout = function() {		
						$('#colorSelector-<?php echo $value['id']; ?>').ColorPicker({
							color: '<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>',
							onShow: function (colpkr) {
								$(colpkr).fadeIn(500);
								return false;
							},
							onHide: function (colpkr) {
								$(colpkr).fadeOut(500);
								return false;
							},
							onChange: function (hsb, hex, rgb) {
								$('#colorSelector-<?php echo $value['id'] ?> div').css('backgroundColor', '#' + hex);
								$("#<?php echo $value['id']; ?>").attr('value', '#' + hex);								
							}
						});
					};	
					EYE.register(initLayout, 'init');
				})(jQuery)
            </script>
			<div id="colorContainer"><div id="colorSelector-<?php echo $value['id']; ?>"><div style="background-color: <?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>"></div></div></div>
            <input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="hidden" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" />  
			          
		    </td>
		</tr>
		<?php
				}

				if ($value['type'] == "select-categories") { 
				
					$pn_categories_obj = get_categories('hide_empty=0');
					$pn_categories = array();
						
					foreach ($pn_categories_obj as $pn_cat) {
						$pn_categories[$pn_cat->cat_ID] = $pn_cat->cat_name;
					}
					$categories_tmp = array_unshift($pn_categories, "All Categories");
		?>
		<tr valign="top"> 
		    <th scope="row"><?php echo $value['name']; ?>:</th>
		    <td>
		        <select style="width:140px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
               <?php foreach ($pn_categories as $option) { ?>
               <option <?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>
               <?php } ?>
           </select><br />
 <span class="setting-description"><?php echo $value['desc']; ?></span>
		    </td>
		</tr>
		<?php
				}	

			}
		?>
		</table>
	</div>
	
	<p class="submit">
	<input name="save" type="submit" class="button-primary" value="Save changes" /> 
	<input type="hidden" name="action" value="save" />
	</p>
	</form>
	
	<?php
	}
	add_action('admin_menu', 'mytheme_add_admin');

?>
