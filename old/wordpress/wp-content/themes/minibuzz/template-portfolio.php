<?php /*
Template Name: Portfolio
*/
?>
<?php get_header(); ?>
<div id="header-inner">
	<?php include_once(TEMPLATEPATH . '/title.php'); ?>
</div><!-- end #header-inner -->
<div id="content-full">
	<div id="gallery-category">
		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<div id="breadcrumbs">','</div>');
		} ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php $values = get_post_custom_values("category-include"); $cat=$values[0];  ?>
		<?php endwhile; endif; ?>
		<?php wp_reset_query();?>
		<ul id="navcat">
			<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-portfolio-navigation.php'); ?>
		</ul>
		<div class="clr"></div>
	</div>
	<div id="main-gallery">
		<?php $postcount = get_option('minibuzz_portfoliopost');?>
		<?php $wp_query = new WP_Query(); ?>
		<?php $strinclude = $cat;?>
		<?php $catinclude = 'cat=' . $strinclude ;?>
		<?php $wp_query->query('&' . $catinclude .' &paged='.$paged .'&showposts='.$postcount); $i=1; ?>
		<ul>
<!--			<li><a rel="portfolio" href="http://www.youtube.com/v/617ANIA5Rqs" title="The Knife: We Share Our Mother's Health">Flash / Video (Iframe/Direct Link To YouTube)</a></li>
-->			<?php while (have_posts()) : the_post();
			$x=1;
			$image = get_post_custom_values("image");
			$thumb = get_post_custom_values("thumb");
			$video = get_post_custom_values("video");

			if(($i%4) == 0){
				$addclass = "nomargin";
			}	
			?>
			<li class="<?php echo $addclass; ?>">
			<div class="portfoliobg">
			
			<?php if($thumb!="" || $image!="" || $video!="" ){ ?>
				<?php if($thumb!=""){ 
				 $imgpath=$thumb[0];
				} else {
				 $imgpath=get_bloginfo('template_url').'/includes/timthumb.php?src=/'.$image[0].'&amp;w=220&amp;h=124&amp;zc=1&amp;q=120';
				}
				?>
				<?php if($video!=""){ ?>
				<a class="video" href="<?php echo $video[0];?>" rel="portfolio" title="<?php the_title();?>"><img src="<?php echo $imgpath; ?>" alt="<?php the_title();?>" class="fade" width="220" height="120" /></a>
				<?php } else { ?>
				<a class="image" href="<?php bloginfo('url'); ?>/<?php echo $image[0];?>" rel="portfolio" title="<?php the_title();?>"><img src="<?php echo $imgpath; ?>" alt="<?php the_title();?>" class="fade" width="220" height="120" /></a>
				<?php } ?>
			<?php } ?>
			</div>
			<span class="tblue"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>" ><?php the_title();?></a></span>
			<?php $excerpt = get_the_excerpt();?>
			<p><?php echo string_limit_words($excerpt,60).'...';?> </p>
			
			</li>
			<?php
			$i++; $addclass = "";
			endwhile; $x++; ?>

			<?php wp_reset_query();	?>
		</ul>
			 <?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagination">
				 <?php wp_pagenavi(); ?>
				 </div>	
			 <?php }else{ ?>
				<div class="navigation">
					<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries', 'minibuzz')) ?></div>
					<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;', 'minibuzz')) ?></div>
				</div>
			<?php }?><br />

	</div><!-- end #main-gallery -->
	<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-portfolio-bottom.php'); ?>
</div><!-- end #content-full -->

<?php get_footer(); ?>
