<?php /*
Template Name: Extra2
*/
?>
<?php get_header(); ?>
<div id="header-inner">
	<?php include_once(TEMPLATEPATH . '/title.php'); ?>
</div><!-- end #header-inner -->
<div id="content">
	<div id="content-left">
		<div id="maintext">
		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<div id="breadcrumbs">','</div>');
		} ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
				<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
				<?php wp_link_pages(array('before' => '<p><strong>' . __('Pages', 'minibuzz') . ':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		</div>
		<?php endwhile; endif; ?>
		<?php edit_post_link(__('Edit this entry.', 'minibuzz'), '<p>', '</p>'); ?>
		<?php comments_template(); ?>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-extra2-right.php'); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->

<?php get_footer(); ?>
