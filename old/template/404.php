<?php get_header(); ?>
<div id="content">
	<div id="content-left">
		<div id="maintext">
				<h2 class="center"><?php _e('Error 404 - Not Found','minibuzz');?></h2>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php get_sidebar(); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->
<?php get_footer(); ?>
