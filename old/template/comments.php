<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'minibuzz');?></p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->

<?php if ( have_comments() ) : ?>
	<strong><?php comments_number(__('No Responses', 'minibuzz'), __('One Response', 'minibuzz'), __('% Responses', 'minibuzz') );?> <?php _e('to','minibuzz');?> &#8220;<?php the_title(); ?>&#8221;</strong>
	<ol class="commentlist">
	<?php wp_list_comments(); ?>
	</ol>
	
	<div class="pagination"><?php paginate_comments_links(); ?></div>
	
	<br style="clear:both" />
	 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<!-- <p class="nocomments">Comments are closed.</p> -->

	<?php endif; ?>
<?php endif; ?>


<?php if ( comments_open() ) : ?>

<div id="respond">

<strong><?php comment_form_title(__('Leave a Reply', 'minibuzz'), __('Leave a Reply to %s', 'minibuzz') ); ?></strong>

<div class="cancel-comment-reply">
	<small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p><?php _e('You must be', 'minibuzz');?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', 'minibuzz');?></a><?php _e('to post a comment.', 'minibuzz');?></p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( is_user_logged_in() ) : ?>

<p><?php _e('Logged in as', 'minibuzz');?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'minibuzz');?>"><?php _e('Log out &raquo;', 'minibuzz');?></a></p>

<?php else : ?>

<p><input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?>  class="inputbox"/>
<label for="author"><small><?php _e('Name', 'minibuzz');?> <?php if ($req) echo "(required)"; ?></small></label></p>

<p><input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> class="inputbox" />
<label for="email"><small><?php _e('Mail (will not be published)', 'minibuzz');?> <?php if ($req) echo "(required)"; ?></small></label></p>

<p><input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" class="inputbox" />
<label for="url"><small><?php _e('Website', 'minibuzz');?></small></label></p>

<?php endif; ?>

<!--<p><small><strong>XHTML:</strong> You can use these tags: <code><?php echo allowed_tags(); ?></code></small></p>-->

<p><textarea name="comment" id="comment" cols="58" rows="10" tabindex="4" class="inputbox"></textarea></p>

<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" class="but" />
<?php comment_id_fields(); ?>
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>
