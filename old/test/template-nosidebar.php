<?php /*
Template Name: No Sidebar
*/
?>
<?php get_header(); ?>
<div id="header-inner">
	<?php include_once(TEMPLATEPATH . '/title.php'); ?>
</div><!-- end #header-inner -->
<div id="content-full">
		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<div id="breadcrumbs">','</div>');
		} ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
			<?php wp_link_pages(array('before' => '<p><strong>' . __('Pages', 'minibuzz') . ':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		<?php endwhile; endif; ?><br style="clear:left" />
		<?php edit_post_link(__('Edit this entry.', 'minibuzz'), '<p>', '</p>'); ?>
		<?php comments_template(); ?>
</div><!-- end #content-full -->

<?php get_footer(); ?>
