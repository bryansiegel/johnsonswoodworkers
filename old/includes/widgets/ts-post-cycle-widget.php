<?php
// =============================== Post Cycle widget ======================================
class TS_CycleWidget extends WP_Widget {
    /** constructor */
    function TS_CycleWidget() {
        parent::WP_Widget(false, $name = 'TS - Post Cycle');	
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
		$limit = apply_filters('widget_title', $instance['limit']);
		$category = apply_filters('widget_category', $instance['category']);
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
							<div class="boxslideshow">
								<?php $querycat = $category;?>
								<?php $limittext = $limit;?>
								<?php global $more;	$more = 0;?>
								<?php query_posts("showposts=-1&cat=" . $querycat);?>
								<?php while (have_posts()) : the_post(); ?>	
								<div class="cycle">
								<?php if($limittext=="" || $limittext==0){
								the_content('<p>' . __('Continue Reading...', 'minibuzz') . '</p>');
								}else{
								$excerpt = get_the_excerpt(); echo string_limit_words($excerpt,$limittext).'...';
								?>
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>">&raquo;</a>						<?php } ?>
								</div>
								<?php endwhile; ?>
							</div>
							<!-- end of boxslideshow -->
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {				
        $title = esc_attr($instance['title']);
		$limit = esc_attr($instance['limit']);
		$category = esc_attr($instance['category']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
			
 <p><label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Limit Text:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit; ?>" /></label></p>
 			
            <p><label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category id Cycle:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo $category; ?>" /></label></p>
        <?php 
    }

} // class Cycle Widget


?>