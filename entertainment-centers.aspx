﻿<%@ Page Title="Riverside Custom Entertainment Centers | Johnsons Woodworkers" Language="C#" MasterPageFile="~/MasterPages/SiteInternal.master" AutoEventWireup="true" CodeFile="entertainment-centers.aspx.cs" Inherits="entertainment_centers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Riverside California Custom entertainment center maker Johnsons wooodworkers can build the custom wood entertainment center of your dreams! Contact us for a free estimate!" />
    <style type="text/css">
        .style1
        {
            width: 416px;
            height: 324px;
        }
        .style2
        {
            width: 367px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" Runat="Server">
    <h1>Custom Entertainment Centers</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <table cellpadding="10" class="boxslideshow">
        <tr>
            <td class="style2" valign="top">
                <img alt="" class="style1" 
                    src="/images/entertainment-centers/entertainment-center-main.jpg" /></td>
            <td valign="top">
                <a class="thumbnail" href="#thumb"><img id="Img1" src="images/entertainment-centers/entertainment-center-1small.jpg" width="100px" height="66px" border="0" /><span><img id="Img2" src="images/entertainment-centers/entertainment-center-1big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img3" src="images/entertainment-centers/entertainment-center-1small.jpg" width="100px" height="66px" border="0" /><span><img id="Img4" src="images/entertainment-centers/entertainment-center-1big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img5" src="images/entertainment-centers/entertainment-center-2small.jpg" width="100px" height="66px" border="0" /><span><img id="Img6" src="images/entertainment-centers/entertainment-center-2big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img7" src="images/entertainment-centers/entertainment-center-3small.jpg" width="100px" height="66px" border="0" /><span><img id="Img8" src="images/entertainment-centers/entertainment-center-3big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img9" src="images/entertainment-centers/entertainment-center-4small.jpg" width="100px" height="66px" border="0" /><span><img id="Img10" src="images/entertainment-centers/entertainment-center-4big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img11" src="images/entertainment-centers/entertainment-center-5small.jpg" width="100px" height="66px" border="0" /><span><img id="Img12" src="images/entertainment-centers/entertainment-center-5big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img13" src="images/entertainment-centers/entertainment-center-6small.jpg" width="100px" height="66px" border="0" /><span><img id="Img14" src="images/entertainment-centers/entertainment-center-6big.jpg" /><br />So real, it's unreal. Or is it?</span></a>

<a class="thumbnail" href="#thumb"><img id="Img15" src="images/entertainment-centers/entertainment-center-7small.jpg" width="100px" height="66px" border="0" /><span><img id="Img16" src="images/entertainment-centers/entertainment-center-7big.jpg" /><br />So real, it's unreal. Or is it?</span></a>
</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidePanel" Runat="Server">
    <h2>Why Go Custom?</h2>
<p>We can build a Custom Entertainment Center that can match what you want. Why settle for a pre made hunk of junk that you'll have to replace in a few years when you can have something that will last the passage of time. Imagine a Entertainment center that is built for your home. Since our work is custom, we can match the wood, height and width so that it will fit perfectly within your home.</p>
</asp:Content>

