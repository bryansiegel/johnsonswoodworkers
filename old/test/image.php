<?php get_header(); ?>
<div id="content">
	<div id="content-left">
		<div id="maintext">
			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div id="breadcrumbs">','</div>');
			} ?>
		  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="post" id="post-<?php the_ID(); ?>">
					<h2><a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment"><?php echo get_the_title($post->post_parent); ?></a> &raquo; <?php the_title(); ?></h2>
					<div class="entry">
						<p class="attachment"><a href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'medium' ); ?></a></p>
						<div class="caption"><?php if ( !empty($post->post_excerpt) ) the_excerpt(); // this is the "caption" ?></div>
		
						<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
		
						<div class="navigation">
							<div class="alignleft"><?php previous_image_link() ?></div>
							<div class="alignright"><?php next_image_link() ?></div>
						</div>
						<br class="clear" />
					</div>
		
				</div>
				<div class="clr"></div>
			<?php comments_template(); ?>
		
			<?php endwhile; else: ?>
		
				<p> <?php _e('Sorry, no attachments matched your criteria.', 'minibuzz'); ?></p>
		
		<?php endif; ?>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php get_sidebar(); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->
<?php get_footer(); ?>
