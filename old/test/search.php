<?php get_header(); ?>
<div id="header-inner">
	<h1 class="pagetitle"><?php _e('Search Result', 'minibuzz');?></h1>
</div><!-- end #header-inner -->
<div id="content">
	<div id="content-left">
		<div id="maintext">
			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div id="breadcrumbs">','</div>');
			} ?>
			<?php if (have_posts()) : ?>
				<div id="searchresult">
				<?php while (have_posts()) : the_post(); ?>
					<div <?php post_class() ?>>
						<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						<?php $excerpt = get_the_excerpt();?>
						<p><?php echo string_limit_words($excerpt,200).'...';?> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>"><?php _e('Continue Reading...', 'minibuzz'); ?></a></p>
						
					</div>
				<?php endwhile; ?>
				</div>
		
			 <?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagination">
				 <?php wp_pagenavi(); ?>
				 </div>	
			 <?php }else{ ?>
				<div class="navigation">
					<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries', 'minibuzz')) ?></div>
					<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;', 'minibuzz')) ?></div>
				</div>
			<?php }?>
		
			<?php else : ?>
		
				<h2 class="center"><?php _e('No posts found. Try a different search?', 'minibuzz');?></h2>
				<?php include_once (TEMPLATEPATH . '/searchform.php'); ?>
		
			<?php endif; ?>
			
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php get_sidebar(); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->

<?php get_footer(); ?>
