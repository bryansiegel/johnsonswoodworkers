<?php
// =============================== TS News  Widget ======================================
class TS_NewsWidget extends WP_Widget {
    /** constructor */
    function TS_NewsWidget() {
        parent::WP_Widget(false, $name = 'TS - News');	
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
		$category = apply_filters('widget_category', $instance['category']);
        ?><div class="news-container">
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
						
								<?php  if (have_posts()) : ?>
								<ul class="newslist">
								<?php $querycat = $category;?>
								<?php query_posts("showposts=3&cat=" . $querycat);?>
								<?php while (have_posts()) : the_post(); ?>	
								<li>
								<span class="small"><?php the_time('F j, Y') ?></span><br />
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>"><strong><?php the_title(); ?></strong></a><br />
								<?php $excerpt = get_the_excerpt(); echo string_limit_words($excerpt,100).'...';?>
								</li>
								<?php endwhile; ?>
								</ul>
								<?php endif; ?>
								<?php wp_reset_query();?>
              <?php echo $after_widget; ?>
			 </div>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {				
        $title = esc_attr($instance['title']);
		$category = esc_attr($instance['category']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'minibuzz'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
			
            <p><label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category id  News:', 'minibuzz'); ?> <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo $category; ?>" /></label></p>
        <?php 
    }

} // class  Widget
?>