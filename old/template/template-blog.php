<?php /*
Template Name: Blog
*/
?>
<?php get_header(); ?>
<div id="header-inner">
	<?php include_once(TEMPLATEPATH . '/title.php'); ?>
</div><!-- end #header-inner -->
<div id="content">
	<div id="content-left">
		<div id="maintext">
		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<div id="breadcrumbs">','</div>');
		} ?>
		<?php $values = get_post_custom_values("category-include"); $cat=$values[0];  ?>
		<?php $numpost = get_option('minibuzz_blogpost');?>
		<?php global $more;	$more = 0;?>
		<?php $wp_query = new WP_Query(); ?>
		<?php $strinclude = $cat;?>
		<?php $catinclude = 'cat=' . $strinclude ;?>
		<?php $wp_query->query('&' . $catinclude .' &paged='.$paged.'&showposts='.$numpost); ?>
		
		<?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'minibuzz');?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<span class="smalltext"><?php _e('Posted by', 'minibuzz');?> <?php the_author() ?> <?php _e('on', 'minibuzz');?> <?php the_time('F j, Y') ?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php edit_post_link(__('Edit', 'minibuzz'), '', '&nbsp;&nbsp;|&nbsp;&nbsp;'); ?> <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></span>
					<div class="entry">
						<?php the_content('' . __('Continue Reading...', 'minibuzz') . ''); ?>
					</div>
				</div><!-- end post -->
	
			<?php endwhile; ?>
	
			 <?php if(function_exists('wp_pagenavi')) { ?>
				<div class="pagination">
				 <?php wp_pagenavi(); ?>
				 </div>	
			 <?php }else{ ?>
				<div class="navigation">
					<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries', 'minibuzz')) ?></div>
					<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;', 'minibuzz')) ?></div>
				</div>
			<?php }?>
	
		<?php else : ?>
			<h2 class="center"><?php _e('Not Found', 'minibuzz');?></h2>
			<p class="center"><?php _e('Sorry, but you are looking for something that isnt here', 'minibuzz');?></p>
			<?php include_once (TEMPLATEPATH . '/searchform.php'); ?>
		<?php endif; ?>
		<?php comments_template(); ?>
		</div><!-- end #maintext -->
	</div><!-- end #content-left -->
	<div id="content-right">
		<div id="sideright">
			<?php include_once(TEMPLATEPATH . '/sidebar/sidebar-blog-right.php'); ?>
		</div><!-- end #sideright -->
	</div><!-- end #content-right -->
	<div class="clr"></div><!-- end clear float -->
</div><!-- end #content -->

<?php get_footer(); ?>
