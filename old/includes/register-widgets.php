<?php
/**
 * Loads up all the widgets defined by this theme. Note that this function will not work for versions of WordPress 2.7 or lower
 *
 */

include_once (TEMPLATEPATH . '/includes/widgets/ts-news-widget.php');
include_once (TEMPLATEPATH . '/includes/widgets/ts-comment-widget.php');
include_once (TEMPLATEPATH . '/includes/widgets/ts-requestquote-widget.php');
include_once (TEMPLATEPATH . '/includes/widgets/ts-post-cycle-widget.php');

add_action("widgets_init", "load_ts_widgets");

function load_ts_widgets() {
	register_widget("TS_NewsWidget");
	register_widget("TS_CommentWidget");
	register_widget("TS_RequestQuoteWidget");
	register_widget("TS_CycleWidget");
}
?>